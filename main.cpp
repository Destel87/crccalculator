#include <iostream>
#include "CRCFile.h"
#include <sys/stat.h>

using namespace std;
int calc(string &source_path, uint32_t &crc32FileRes );
int loadFileName(string& sAbsFileNamePath);

int main (int argc,char *argv[])
{
    string name;
//    argv[1] = "FwCore";
    name.append(argv[1]);

    uint32_t crc32;
    calc(name, crc32);
    return 0;
}

//int checkFile(string &source_path, uint32_t &ulFileSize, uint16_t &crc16Res, uint32_t ulcrc32ToCheck )
int calc(string &source_path, uint32_t &crc32FileRes )
{
    int   err;
    unsigned long crc32file;

    /////////////////////////////
    //check file to upgrade
    CRCFile fileImageCrc;
//    loadFileName(source_path);
//    if (fileImage.exists())

        //check crc of the file
        err=fileImageCrc.calculateCRCFileMaxAlloc(source_path, crc32file, CRCFile::eMaxAllocMemoryFile);
        if (err==0)
        {
            crc32FileRes = crc32file;
            printf("%x\n", crc32file);
        }
        else
        {
            return -1;
        }

//        printf("FwUpgrade::checkFile--> file crc:0x%X\n", crc32file);
    return 0;
}
string m_sFilename;
string m_sAbsoluteFilename;
string m_sFilePath;
bool m_isaFile;
        static const string m_fs;

bool isDir(void)
{
    struct stat statBuf;
    if ( stat(m_sAbsoluteFilename.c_str(), &statBuf) == -1)
    {
        // wrong path or sys error
        return false;
    }

    if ( S_ISDIR(statBuf.st_mode) )
    {
        return true;
    }
    return false;
}

int loadFileName(string& sAbsFileNamePath)
{
    if ( sAbsFileNamePath.empty() )		return -1;

    m_sAbsoluteFilename = sAbsFileNamePath;
    if ( isDir() )
    {
        m_sFilePath = sAbsFileNamePath;
        m_isaFile = false;
        return 2;
    }
    else
    {
        size_t usPos = sAbsFileNamePath.rfind(m_fs, sAbsFileNamePath.length());
        if ( usPos != string::npos )
        {
            m_sFilename = sAbsFileNamePath.substr(usPos + 1, sAbsFileNamePath.length() - usPos);
            m_sFilePath = sAbsFileNamePath.substr(0, usPos);
            m_isaFile = true;
            return 1;
        }
        // assign value of absolutefilenamepath to filename if it is only fileName
        if ( sAbsFileNamePath.length() > 0 )
        {
            m_sFilename = sAbsFileNamePath;
            m_isaFile = true;
            return 1;
        }
    }
    return -1;
}

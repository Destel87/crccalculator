TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        CRCFile.cpp \
        main.cpp

TARGET = crc32calc

# deployment directives
target.path = /home/root
INSTALLS += target

HEADERS += \
    CRCFile.h
